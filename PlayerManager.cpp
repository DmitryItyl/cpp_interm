#include "PlayerManager.h"

int idPlayerLimit{ 0 };


void PlayerManager::CreatePlayer(std::string name)
{
    int rank = 0;
    int id = idPlayerLimit++;

    Player* newPlayer = new Player(name, rank, id);
    players.push_back(newPlayer);
}


Player PlayerManager::GetPlayerById(int id)
{
    for (auto player : players)
    {
        if (id == player->id)
            return *player;
    }
    throw "No player with specified id found in the game";
}

Player PlayerManager::GetPlayerByName(std::string name)
{
    for (auto player : players)
    {
        if (name == player->name)
            return *player;
    }
    throw "No player with specified name found in the game";
}


void PlayerManager::ShowPlayerInfo(Player player)
{
    std::cout << player.id << ". " << player.name << ":   rank  " << player.rank << std::endl;
}


void PlayerManager::DeletePlayer(Player delPlayer)
{
    for (size_t i = 0; i < players.size(); i++)
    {
        if (players[i]->id == delPlayer.id)
            players.erase(players.begin() + i);
    }
}