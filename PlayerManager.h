#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <conio.h>

class Player
{
public:
    std::string name;
    int id;
    int rank;

    Player(std::string _name, int _rank, int _id)
    {
        name = _name;
        rank = _rank;
        id = _id;
    }
};


class PlayerManager
{
public:

    std::vector<Player*> players;

    void CreatePlayer(std::string name);
    Player GetPlayerById(int id);
    Player GetPlayerByName(std::string name);
    void ShowPlayerInfo(Player player);
    void DeletePlayer(Player delPlayer);
};


