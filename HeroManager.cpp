#include "HeroManager.h"

int idHeroLimit{ 0 };

void HeroManager::CreateHero(std::string name, int hp, int damage)
{
    int id = idHeroLimit++;

    Hero newHero = Hero(name, id, hp, damage);

    heroes.push_back(newHero);
}

Hero HeroManager::GetHeroById(int id)
{
    for (auto hero : heroes)
    {
        if (id == hero.id)
            return hero;
    }
    throw "No hero with specified id found in the game";
}

Hero HeroManager::GetHeroByName(std::string name)
{
    for (auto hero : heroes)
    {
        if (name == hero.name)
            return hero;
    }
    throw "No hero with specified name found in the game";
}

void HeroManager::ShowHeroInfo(Hero hero)
{
    std::cout << hero.id << ". " << hero.name << ":   HP: " << hero.hp << ";  DMG: " << hero.damage << std::endl;
}


void HeroManager::DeleteHero(Hero delHero)
{
    for (size_t i = 0; i < heroes.size(); i++)
    {
        if (heroes[i].id == delHero.id)
            heroes.erase(heroes.begin() + i);
    }
}