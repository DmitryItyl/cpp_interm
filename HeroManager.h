#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <conio.h>

class Hero
{
public:
    std::string name;
    int id;
    int hp;
    int damage;

    Hero(std::string _name, int _id, int _hp, int _damage)
    {
        name = _name;
        id = _id;
        hp = _hp;
        damage = _damage;
    }
};


class HeroManager
{
public:
    std::vector<Hero> heroes;

    void CreateHero(std::string name, int hp, int damage);
    void DeleteHero(Hero delHero);
    void ShowHeroInfo(Hero hero);
    Hero GetHeroById(int id);
    Hero GetHeroByName(std::string name);
};