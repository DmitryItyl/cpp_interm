#pragma once

#include <iostream>
#include <vector>
#include <string>
#include <conio.h>

#include "PlayerManager.h"
#include "HeroManager.h"

class Team
{

public:
    std::string name;
    std::vector<std::pair<Player*, Hero>> members;

    Team(std::string _name, std::vector<Player*> _players, std::vector<Hero> _heroes)
    {
        name = _name;
        for (size_t i = 0; i < 5; i++)
        {
            members.push_back(std::make_pair(_players[i], _heroes[i]));
        }
    }

    Team() { }
};

class TeamManager
{
public:
    std::vector<Team> teams;
    
    Team GenerateNewTeam(std::string name, std::vector<Player*>* players, std::vector<Hero>* heroes);
    void ShowTeamInfo(Team team);
};



