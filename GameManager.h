#pragma once
#include <stdlib.h>
#include <chrono>

#include"TeamManager.h"

class Session
{
public:
	std::chrono::time_point<std::chrono::system_clock> startTime;
	Team team1;
	Team team2;
	Team winner;

	Session(Team _team1, Team _team2)
	{
		team1 = _team1;
		team2 = _team2;
		startTime = std::chrono::system_clock::now();
	}

	void CalculateWinner();
};


class GameManager
{
public:
	std::vector<Session> gameSessions;
	PlayerManager playerManager = PlayerManager();
	HeroManager heroManager = HeroManager();
	TeamManager teamManager = TeamManager();

	GameManager()
	{
		heroManager.CreateHero("Mudd the Gobbo", 12, 5);
		heroManager.CreateHero("Bob the Brute", 5, 15);
		heroManager.CreateHero("Marco the Mago", 2, 25);
		heroManager.CreateHero("Billy the Bandit", 10, 10);
		heroManager.CreateHero("Robbert the Poet", 2, 2);
		heroManager.CreateHero("Candice the Princess", 4, 5);
		heroManager.CreateHero("Eric the Cleric", 12, 10);
		heroManager.CreateHero("Frank the Tank", 22, 1);
		heroManager.CreateHero("Butch the Butcher", 12, 15);
		heroManager.CreateHero("Gilbert the Lombard", 11, 9);

		playerManager.CreatePlayer("Ben");
		playerManager.CreatePlayer("Sarah");
		playerManager.CreatePlayer("Bob");
		playerManager.CreatePlayer("Rich");
		playerManager.CreatePlayer("Steve");
		playerManager.CreatePlayer("Denny");
		playerManager.CreatePlayer("Mark");
		playerManager.CreatePlayer("Shawn");
		playerManager.CreatePlayer("Kate");
		playerManager.CreatePlayer("Anne");

	}

	void PerformGameSession();
};

