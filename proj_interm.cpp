// proj_interm.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>
#include <string>
#include <conio.h>

#include "GameManager.h"


void AddPlayer(GameManager game);
void DelPlayer(GameManager game);
void ShowPlayers(GameManager game);


int main()
{
    GameManager game;

    // impementing simple menu

    std::cout << "Welcome to the game!" << std::endl << "==============================" << std::endl;

    while (true)
    {
        std::cout << "1. Add new player" << std::endl;
        std::cout << "2. Delete player" << std::endl;
        std::cout << "3. Show all players" << std::endl;
        std::cout << "4. Play!" << std::endl;

        int menuKey;
        std::cout << ">> " << std::flush;
        std::cin >> menuKey;

        try {

            switch (menuKey)
            {
            case 1:
            {
                AddPlayer(game);
                break;
            }

            case 2:
            {
                DelPlayer(game);
                break;
            }
            case 3:
            {
                ShowPlayers(game);
                break;
            }
            case 4:
            {
                game.PerformGameSession();
                break;
            }

            default:
                std::cout << "Error: unknown command. Press any key to continue..." << std::endl;
                break;
            }

        }
        catch(const char* exception)
        {
            std::cout << "Execution failed because of the error: " << std::endl << exception << std::endl;
        }

        _getch();
        system("CLS");
    }


    return 0;
}


void AddPlayer(GameManager game)
{
    std::cout << "Enter player's name: " << std::flush;
    std::string addName;
    std::cin >> addName;
    game.playerManager.CreatePlayer(addName);
    std::cout << addName << " has been successfully added to the game! Press any key to continue..." << std::endl;
}

void DelPlayer(GameManager game)
{
    std::cout << std::endl << "1. Search player by name" << std::endl;
    std::cout << "2. Search player by id" << std::endl;
    int delSubKey;
    std::cin >> delSubKey;

    if (delSubKey == 1)
    {
        std::cout << "Which player do you want to delete?" << std::endl << ">> " << std::flush;
        std::string delName;
        std::cin >> delName;
        Player delPlayer = game.playerManager.GetPlayerByName(delName);
        game.playerManager.DeletePlayer(delPlayer);
        std::cout << delPlayer.name << " has been successfully removed from the game. Press any key to continue..." << std::endl;
    }
    else if (delSubKey == 2)
    {
        std::cout << "Which player do you want to delete?" << std::endl << ">> " << std::flush;
        int delId;
        std::cin >> delId;
        Player delPlayer = game.playerManager.GetPlayerById(delId);
        game.playerManager.DeletePlayer(delPlayer);
        std::cout << delPlayer.name << " has been successfully removed from the game. Press any key to continue..." << std::endl;
    }
    else
    {
        std::cout << "Error: unknown command. Press any key to continue..." << std::endl;
    }
}

void ShowPlayers(GameManager game)
{
    std::cout << "All players in the game" << std::endl;
    for (auto player : game.playerManager.players)
    {
        game.playerManager.ShowPlayerInfo(*player);
    }
    std::cout << "Press any key to continue..." << std::endl;
}