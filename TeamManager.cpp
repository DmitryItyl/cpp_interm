#include "TeamManager.h"


Team TeamManager::GenerateNewTeam(std::string name, std::vector<Player*>* players, std::vector<Hero>* heroes)
{
	if ((players->size() < 5) || (heroes->size() < 5))
	{
		throw "Not enough players/heroes to generate a team";
	}

	std::vector<Player*> selectedPlayers;
	std::vector<Hero> selectedHeroes;

	for (size_t i = 0; i < 5; i++)
	{
		int playerNum = std::rand() % players->size();
		int heroNum = std::rand() % heroes->size();

		selectedPlayers.push_back((*players)[playerNum]);
		selectedHeroes.push_back((*heroes)[heroNum]);

		players->erase(players->begin() + playerNum);
		heroes->erase(heroes->begin() + heroNum);
	}


	Team newTeam = Team(name, selectedPlayers, selectedHeroes);

	teams.push_back(newTeam);

	return newTeam;
}


void TeamManager::ShowTeamInfo(Team team)
{
	std::cout << "    Team:    " << team.name << "    " << std::endl;
	std::cout << "   Player:                         Hero:        " << std::endl;

	for (int i = 0; i < team.members.size(); i++)
	{
		std::cout << "[" << i + 1 << "]:  " << team.members[i].first->name << ", rank  " << team.members[i].first->rank << ", playing as  " << team.members[i].second.name << std::endl;
	}

}