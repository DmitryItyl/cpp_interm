#include "GameManager.h"


void Session::CalculateWinner()
{
	int team1Result{ 0 };
	int team2Result{ 0 };
	winner.name = "";

	for (size_t i = 0; i < 5; i++)
	{
		team1Result += team1.members[i].second.hp - team2.members[i].second.damage;

		team2Result += team2.members[i].second.hp - team1.members[i].second.damage;
	}
	
	Team loser;
	if (team1Result > team2Result)
	{
		winner = team1;
		loser = team2;
	}
	else if (team1Result < team2Result)
	{
		winner = team2;
		loser = team1;
	}

	for (size_t i = 0; i < 5; i++)
	{
		winner.members[i].first->rank += 25;

		if (loser.members[i].first->rank > 25)
			loser.members[i].first->rank -= 25;
		else
			loser.members[i].first->rank = 0;
	}
}


void GameManager::PerformGameSession()
{
	std::vector<Player*>* playersCopy = new std::vector<Player*>(playerManager.players);
	std::vector<Hero>* heroesCopy = new std::vector<Hero>(heroManager.heroes);

	Team team1 = teamManager.GenerateNewTeam("Red team", playersCopy, heroesCopy);
	Team team2 = teamManager.GenerateNewTeam("Blue team", playersCopy, heroesCopy);

	Session newSession(team1, team2);

	gameSessions.push_back(newSession);

	teamManager.ShowTeamInfo(team1);
	teamManager.ShowTeamInfo(team2);

	newSession.CalculateWinner();

	if (newSession.winner.name == "")
		std::cout << "It's a draw! No one gets points" << std::endl;
	else
		std::cout << "The winner is: " << newSession.winner.name << "!!" << std::endl << "Every one of its memebers gets 25 points! The losing team loses 25 points." << std::endl;
}